package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class DetailsServlet extends HttpServlet {

	public void init() throws ServletException {
		System.out.println("**************************************");
		System.out.println(" DetailsServlet has been initialized. ");
		System.out.println("**************************************");
	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
		
		ServletContext srvContext = getServletContext();
		String branding = srvContext.getInitParameter("branding");
		
		// System Properties
		String firstName = System.getProperty("firstName");
		
		// HttpSession
		HttpSession session = req.getSession();
		String lastName = session.getAttribute("lastName").toString();

		// Servlet Context
		String contact = srvContext.getAttribute("contact").toString();
		
		// URL rewriting via sendRedirect()
		String email = req.getParameter("email");
		
		PrintWriter out = res.getWriter();
		
		out.println(
			"<h1>Welcome to " + branding + "</h1>" +
			"<p>First Name: " + firstName + "</p>" +
			"<p>Last Name: " + lastName + "</p>" +
			"<p>Contact: " + contact + "</p>" +
			"<p>Email: " + email + "</p>"
		);
		
	}
	
	public void destroy() {
		System.out.println("************************************");
		System.out.println(" DetailsServlet has been destroyed. ");
		System.out.println("************************************");
	}
	
}
