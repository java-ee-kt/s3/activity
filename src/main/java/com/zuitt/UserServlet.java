package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class UserServlet extends HttpServlet {

	public void init() throws ServletException {
		System.out.println("**************************************");
		System.out.println(" UserServlet has been initialized. ");
		System.out.println("**************************************");
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
		
		String firstName = req.getParameter("firstName");
		String lastName = req.getParameter("lastName");
		String email = req.getParameter("email");
		String contact = req.getParameter("contact");
		
		// System Properties
		System.getProperties().put("firstName", firstName);
		// String facilities = System.getProperty("facilities");
		
		// HttpSession
		HttpSession session = req.getSession();
		session.setAttribute("lastName", lastName);

		// Servlet Context
		ServletContext srvContext = getServletContext();
		srvContext.setAttribute("contact", contact);
		
		// URL rewriting via sendRedirect()
		res.sendRedirect("details?email=" + email);
		
	}
	
	public void destroy() {
		System.out.println("************************************");
		System.out.println(" UserServlet has been destroyed. ");
		System.out.println("************************************");
	}
	
}
